import images
def pixelate(image, pixel_size):
    pixelated = [[0 for x in range(len(image[0]))] for y in range(len(image))]
    div = pixel_size * pixel_size
    for i in range(0, len(image), pixel_size):
        for j in range(0, len(image[0]), pixel_size):

            r_sum = 0
            g_sum = 0
            b_sum = 0

            for k in range(i, min(i + pixel_size, len(image))):
                for l in range(j, min(j + pixel_size, len(image[0]))):
                    r, g, b = image[k][l]

                    r_sum += r
                    g_sum += g
                    b_sum += b

            r_avg = r_sum // div
            g_avg = g_sum // div
            b_avg = b_sum // div

            for m in range(i, min(i + pixel_size, len(image))):
                for n in range(j, min(j + pixel_size, len(image[0]))):
                    pixelated[m][n] = (r_avg, g_avg, b_avg)

    return pixelated
def main():
    imagen = input("Nombre de la imagen: ")
    t_pix = int(input("Tamaño de pixelado: "))
    img = images.read_img(imagen)
    pixelated_img = pixelate(img, t_pix)
    images.write_img(pixelated_img, imagen + "_pixelated.png")

if __name__ == '__main__':
    main()