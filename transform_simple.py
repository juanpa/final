import images
import transforms
def main():
    imagen = input("Introduce el nombre de la imagen en formato RGB: ")
    funcion = input("Introduce la funcion que quieres aplicar: rotate_right, mirror, blur, grayscale: ")

    img = images.read_img(imagen)
    if funcion == "rotate_right":
        imagen_cambiada = transforms.rotate_right(img)

    if funcion == "mirror":
        imagen_cambiada = transforms.mirror(img)

    if funcion == "blur":
        imagen_cambiada = transforms.blur(img)

    if funcion == "grayscale":
        imagen_cambiada = transforms.grayscale(img)

    images.write_img(imagen_cambiada, imagen + "_trans.png")

if __name__ == '__main__':
    main()
