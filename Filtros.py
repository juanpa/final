import images
import transforms
def main():
    print("Retoques de imagen: \nAumentar Brillo \nDisminuir Brillo \nAplicar filtros")
    imagen = input("Nombre de la imagen: ")
    retoque = input("¿Que retoque quieres aplicar a la imagen?: ")
    img = images.read_img(imagen)
    if retoque == "Aumentar Brillo":
        aumento = int(input("¿Cuanto quieres aumentar el brillo (1-3)?: "))
        if aumento == 1:
            filtered_img = transforms.filter(img, 1.2, 1.2, 1.2)
        elif aumento == 2:
            filtered_img = transforms.filter(img, 1.4, 1.4, 1.4)
        elif aumento == 3:
            filtered_img = transforms.filter(img, 1.6, 1.6, 1.6)
        images.write_img(filtered_img, imagen + "_filter.png")
    if retoque == "Disminuir Brillo":
        disminucion = (input("¿Cuanto quieres disminuir el brillo (1-3)?: "))
        if disminucion == 1:
            filtered_img = transforms.filter(img, 0.8, 0.8, 0.8)
        elif disminucion == 2:
            filtered_img = transforms.filter(img, 0.6, 0.6, 0.6)
        elif disminucion == 3:
            filtered_img = transforms.filter(img, 0.4, 0.4, 0.4)
        images.write_img(filtered_img, imagen + "_filter.png")
    if retoque == "Aplicar filtros":
        filtro = input("Filtro a aplicar (Rojo, Verde, Azul, Lipstick, Clarendon, blanco y negro): ")
        if filtro == "Rojo":
            filtered_img = transforms.filter(img, 1.7, 1, 1)
        elif filtro == "Verde":
            filtered_img = transforms.filter(img, 1, 1.7, 1)
        elif filtro == "Azul":
            filtered_img = transforms.filter(img, 1, 1, 1.7)
        elif filtro == "Lipstick":
            filtered_img = transforms.filter(img, 1.5, 1, 1.5)
        elif filtro == "Clarendon":
            filtered_img = transforms.filter(img, 1.3, 1.2, 0.7)
        elif filtro == "blanco y negro":
            filtered_img = transforms.grayscale(img)
        images.write_img(filtered_img, imagen + "_filter.png")

if __name__ == '__main__':
    main()
