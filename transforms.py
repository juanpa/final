def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple, to_change_to: tuple):
    changed_colors_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]
    for x in range(len(image)):
        for y in range(len(image[x])):
            changed_colors_image[x][y] = image[x][y]
            if changed_colors_image[x][y] == to_change:
                changed_colors_image[x][y] = to_change_to
    return changed_colors_image
def rotate_right(image: list[list[tuple[int, int, int]]]):
    # Rotar imagen 90 grados
    rotated_image = [[0 for x in range(len(image))] for y in range(len(image[0]))]
    for x in range(len(image)):
        for y in range(len(image[x])):
            rotated_image[y][len(image)-1-x] = image[x][y]
    return rotated_image
def mirror(image: list[list[tuple[int, int, int]]]):
    # Reflejar imagen hoizontalmente
    mirrored_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]
    for x in range(len(image)):
        for y in range(len(image[x])):
            mirrored_image[len(image)-x-1][y] = image[x][y]
    return mirrored_image
def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int):
    # Rotar los colores de cada pixel
    rotated_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]
    for x in range(len(image)):
        for y in range(len(image[x])):
            r, g, b = image[x][y]
            r = (r + increment) % 256
            g = (g + increment) % 256
            b = (b + increment) % 256
            rotated_image[x][y] = (r, g, b)
    return rotated_image

def blur(image):
    # hacer imagen borrosa
    blurred_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]

    for x in range(len(image)):
        for y in range(len(image[x])):

            red = 0
            green = 0
            blue = 0
            count = 0

            # pixel de la izq
            if y - 1 >= 0:
                r, g, b = image[x][y - 1]
                red += r
                green += g
                blue += b
                count += 1

            # pixel de la derecha
            if y + 1 < len(image[x]):
                r, g, b = image[x][y + 1]
                red += r
                green += g
                blue += b
                count += 1

            # pixel de encima
            if x - 1 >= 0:
                r, g, b = image[x - 1][y]
                red += r
                green += g
                blue += b
                count += 1

            # pixel de abajo
            if x + 1 < len(image):
                r, g, b = image[x + 1][y]
                red += r
                green += g
                blue += b
                count += 1

            blurred_image[x][y] = (int(red / count), int(green / count), int(blue / count))

    return blurred_image
def shift(image: list[list[tuple[int, int, int]]], horizontal: tuple, vertical: tuple):
    # desplazar imagen horizontal y verticalmente
    shifted_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]
    for x in range(len(image)):
        for y in range(len(image[x])):
            new_x = (x + vertical) % len(image)
            new_y = (y + horizontal) % len(image[0])
            shifted_image[new_x][new_y] = image[x][y]
    return shifted_image
def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int):
    # cortar imagen
    cropped_image = [[0 for i in range(width)] for j in range(height)]
    for i in range(height):
        for j in range(width):
            if x+j < len(image[0]) and y+i < len(image):
                cropped_image[i][j] = image[y+i][x+j]
    return cropped_image
def grayscale(image: list[list[tuple[int, int, int]]]):
    # poner imagen en blanco y negro
    grayscale_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]
    for x in range(len(image)):
        for y in range(len(image[x])):
            r, g, b = image[x][y]
            gray = int((r + g + b)/3)
            grayscale_image[x][y] = (gray, gray, gray)
    return grayscale_image
def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float):
    # aplicar filtro de color a la imagen
    filtered_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]
    for x in range(len(image)):
        for y in range(len(image[x])):
            orig_r, orig_g, orig_b = image[x][y]
            new_r = int(orig_r * r)
            new_g = int(orig_g * g)
            new_b = int(orig_b * b)
            if new_r > 255:
                new_r = 255
            if new_g > 255:
                new_g = 255
            if new_b > 255:
                new_b = 255
            filtered_image[x][y] = (new_r, new_g, new_b)
    return filtered_image